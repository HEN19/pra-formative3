public interface method {
    void boil();
    void deepFry();
    void flambe();
    void slowCook();
    void steam();
}