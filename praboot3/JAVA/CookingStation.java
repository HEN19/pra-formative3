import java.util.List;

public class CookingStation {
    private String station;
    private int time;
    private List<String> employees;

    public CookingStation(String station, int time, List<String> employees) {
        this.station = station;
        this.time = time;
        this.employees = employees;
    }

    public String getStation() {
        return station;
    }

    public int getTime() {
        return time;
    }

    public List<String> getEmployees() {
        return employees;
    }
}