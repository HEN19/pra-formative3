import java.util.List;

public class CookingSection extends CookingStation implements ingredients, method, cook {
    CookingSection(String station, int time, List<String> employees) {
        super(station, time, employees);
        
    }


    @Override
    public int redMeat(int stock) {
        // Implementation
        return stock;
    }

    @Override
    public int fish(int stock) {
        // Implementation
        return stock;
    }

    @Override
    public void liquid() {
        // Implementation
        System.out.println("No Ingredient used from Liquid");
    }

    @Override
    public void boil() {
        // Implementation
        System.out.println("Boiling.........");
    }

    @Override
    public void deepFry() {
        // Implementation
        System.out.println("Frying.........");
    }

    @Override
    public void flambe() {
        // Implementation
        System.out.println("Burning.........");
    }

    @Override
    public void slowCook() {
        // Implementation
        System.out.println("Cooking.........");
    }

    @Override
    public void steam() {
        // Implementation
        System.out.println("Steam up.........");
    }

    @Override
    public void makeDish() {
        // Implementation
        System.out.println("Wait.........");
    }

    @Override
    public void preCook() {
        // Implementation
        System.out.println("Prepairing.........");
    }

    @Override
    public void garnish() {
        // Implementation
        System.out.println("Get Ready.........");
    }
}
