public interface cook {

    void makeDish();
    void preCook();
    void garnish();
}