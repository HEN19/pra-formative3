import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CookingTest {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Collect information for employee names
        List<String> employeeNames = new ArrayList<>();
        System.out.println("++++++++++++++++++----------+++++++++++++++++++");
        for (int i = 0; i < 2; i++) {
            System.out.println("Enter name for Employee " + (i + 1) + ":");
            String name = scanner.nextLine();
            System.out.println("++++++++++++++++++----------+++++++++++++++++++");
            Employee employee = new Employee(name);
            employeeNames.add(employee.getName());
        }
 

        // Create CookingSection using its constructor
    
        System.out.print("Enter Cooking Station: ");
        String station = scanner.nextLine();

        System.out.print("Enter Cooking Time (minutes): ");
        int time = scanner.nextInt();
        scanner.nextLine(); // Consume the newline
        System.out.println("++++++++++++++++++----------+++++++++++++++++++");

        CookingSection cookingSection = new CookingSection(station, time,employeeNames);

        // // Display Cooking Section Information
    
        System.out.println("Cooking Section: " + cookingSection.getStation());
        System.out.println("Time: " + cookingSection.getTime() + " minutes");
        System.out.println("++++++++++++++++++----------+++++++++++++++++++");

        
        System.out.println("Assigned Employees:");
        for (String employeeName : employeeNames) {
            System.out.println("Name: " + employeeName);
        }
        System.out.println("++++++++++++++++++----------+++++++++++++++++++");

        // // Perform cooking tasks
        System.out.println("++++++++++++++++++----------+++++++++++++++++++");
        System.out.print("Enter Meat Ingrediant: ");
        int redMeat = scanner.nextInt();

        System.out.println("++++++++++++++++++----------+++++++++++++++++++");
        System.out.print("Enter fish Ingrediant: ");
        int fish = scanner.nextInt();

        System.out.println("++++++++++++++++++----------+++++++++++++++++++");

        System.out.println("we cook " + cookingSection.redMeat(redMeat) + " pieces of meats");
        System.out.println("we cook " + cookingSection.fish(fish)+ " slices of fish");
        cookingSection.preCook();
        cookingSection.boil();
        cookingSection.deepFry();
        cookingSection.slowCook();
        cookingSection.flambe();
        cookingSection.steam();
        cookingSection.garnish();
        cookingSection.makeDish();

        System.out.println("Cooking process completed!");
    }
}
